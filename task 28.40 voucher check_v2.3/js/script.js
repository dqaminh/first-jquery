
var gREADY_STATE_REQUEST_DONE = 4;
var gSTATUS_REQUEST_DONE = 200;

$( document ).ready(function() {
  $("#btn-test").on("click",function () {
    console.log("OK");
    var vVoucherCodeElement = $("#voucher");
    var vVoucherCode = vVoucherCodeElement.val();
    
    var vIsValidateData = validateData(vVoucherCode);
    
    if(vIsValidateData) {
    
      var bXmlHttp = new XMLHttpRequest();
      sendVoucherToServer(vVoucherCode, bXmlHttp);
     
      bXmlHttp.onreadystatechange = function() {
        if(bXmlHttp.readyState === gREADY_STATE_REQUEST_DONE
          && bXmlHttp.status === gSTATUS_REQUEST_DONE) {
            processResponse(bXmlHttp);
          }
      }
    }
  })
  
});


function validateData(paramVoucher) {
  var vResultCheckElement = $("#div-result-check");
  if(paramVoucher === "") {
    
    vResultCheckElement.html("Mã giảm giá chưa nhập!");
    
    vResultCheckElement.addClass("text-danger");
    return false;
  }
  
  
  vResultCheckElement.addClass("text-dark");
  return true;
}


function sendVoucherToServer(paramVoucher, paramXmlVoucherRequest) {
  paramXmlVoucherRequest.open("GET", "http://203.171.20.210:8080/devcamp-pizza365/voucher_detail/" + paramVoucher, true);
  paramXmlVoucherRequest.send();
}


function processResponse(paramXmlHttp) {
  
  var vJsonVoucherResponse = paramXmlHttp.responseText;
  
  var vVoucherResObj = JSON.parse(vJsonVoucherResponse); 
  console.log(vJsonVoucherResponse);
  
  var vDiscount = vVoucherResObj.phanTramGiamGia;
  var vResultCheckElement = $("#div-result-check");
  
  if(vDiscount === "-1") {  
    vResultCheckElement.html("Không tồn tại mã giảm giá");
  } 
  else {
    vResultCheckElement.html("Mã giảm giá " + vDiscount +"%"); 
  } 
}